# Font resource generation

## EvanW + SDF

### Manual generation

Online: <https://evanw.github.io/font-texture-generator/>

1. Pick font name
2. Pick mods
3. Set size (`64` is fine for regular text)
4. Set resolution to "Power-of-2"
5. Leave data format at "JSON"
6. Copy-paste <characters.txt> to "Custom:"
7. Pick display type "Distance field"
8. Set falloff to `5` (border effects VS sharpness trade-off)
9. Generate
10. Download PNG to `resources.in/fonts/evanw-sdf/[name]-[size]_[falloff].png`
11. Copy-paste JSON to a matching name

### Compression

Patched `basisu`: <https://gitlab.com/dpwiz/basis_universal> (for `-only`)

1. Generate supercompressed basis in temporary dir:
  ```
  basisu -q 255 -uastc -uastc_level 3 ../../../resources.in/fonts/evanw-sdf/Ubuntu-32_5.png
  ```
2. Transcode to `BC4_R`:
  ```
  basisu -only 4 `Ubuntu-32_5.basis`
  ```
3. Rename resulting file to `Ubuntu-32_5.ktx`
3. Compress with `Zstd`:
  ```
  zstd -T16 -19 Ubuntu-32_5.ktx
  ```
3. Put resulting `.ktx.zst` in `resources/` and purge temporary files (`.basis` and unpacked `.png`).
