# Sky Playground

(was: [playground4])

A game engine (-ish) thingy to go beyound drawing a static triangle.

![screenshot](https://i.imgur.com/uJGmu17.png)

[playground4]: https://gitlab.com/dpwiz/playground4

## Assets used

- «Viking Room» by [nigelgoh] (CC BY 4.0)
- «Flying world - Battle of the Trash god» by [burunduk] (CC BY 4.0)
- «stylised sky player home dioroma» by [Sander Vander Meiren] (CC BY 4.0)

[nigelgoh]: https://sketchfab.com/3d-models/viking-room-a49f1b8e4f5c4ecf9e1fe7d81915ad38
[burunduk]: https://sketchfab.com/3d-models/flying-world-battle-of-the-trash-god-350a9b2fac4c4430b883898e7d3c431f
[Sander Vander Meiren]: https://sketchfab.com/3d-models/stylised-sky-player-home-dioroma-6597e6c9a5184f07a638ac33c08c2ad5
