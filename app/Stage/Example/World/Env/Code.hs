module Stage.Example.World.Env.Code
  ( set1binding0
  ) where

import Render.Code (Code(..), trimming)

set1binding0 :: Code
set1binding0 = Code
  [trimming|
    layout(set=1, binding=0) uniform Env {
      vec4 location;
      vec4 bodies;
      vec4 rayleigh;
      vec4 mie;
      vec4 fancy;
    } env;
  |]
