{-# LANGUAGE OverloadedLists #-}

{-# OPTIONS_GHC -fplugin Foreign.Storable.Generic.Plugin #-}

module Stage.Example.World.Env
  ( Input(..)
  , Env
  , Process
  , spawn

  , Buffer
  , Observer
  , newObserver
  ) where

import RIO

import Control.Monad.Trans.Resource (MonadResource, ResourceT)
import Data.Type.Equality (type (~))
import Foreign.Storable.Generic (GStorable)
import Geomancy (Vec4, vec4)
import Vulkan.Core10 qualified as Vk

import Engine.Types qualified as Engine
import Engine.Worker qualified as Worker
import Resource.Buffer qualified as Buffer

type Process = Worker.Cell Input Env

data Input = Input
  { azimuth     :: Float
  , inclination :: Float
  , altitude    :: Float

  , earthRadius  :: Float
  , atmoHeight   :: Float
  , atmoRayleigh :: Float
  , atmoMie      :: Float
  }
  deriving (Eq, Show)

data Env = Env
  { envLocation :: Vec4 -- ^ Cartesian XYZ + altitude
  , envBodies   :: Vec4 -- ^ Planet radius, atmo height. Sun radius, distance.
  , envRayleigh :: Vec4 -- ^ Beta RGB + altitude scale
  , envMie      :: Vec4 -- ^ Beta RGB + altitude scale
  , envFancy    :: Vec4 -- ^ Ground diffuse+specular, cloud density, 2*unused
  } deriving (Show, Generic)

instance GStorable Env

spawn
  :: ( MonadResource m
     , MonadUnliftIO m
     )
  => Input
  -> m Process
spawn = Worker.spawnCell mkEnv

mkEnv :: Input -> Env
mkEnv Input{..} = Env
  { envLocation =
      vec4
        (sincl * cazim)
        (-cincl)
        (sincl * sazim)
        altitude
  , envBodies =
      vec4
        earthRadius -- earthRadius, km (6360)
        atmoHeight -- atmoHeight (60)
        2772960 -- distance to sun
        1.496e8 -- sun radius
  , envRayleigh =
      vec4
        5.8e-4
        1.35e-3
        3.31e-3
        atmoRayleigh -- 6.0
  , envMie =
      vec4
        4.0e-3
        4.0e-3
        4.0e-3
        atmoMie -- 1.2
  , envFancy =
      vec4
        2 -- ground diffuse/spec id
        0 -- XXX: unused
        3 -- clouds density id
        (atmoHeight / 5) -- clouds altitude (5)
  }
  where
    sincl = sin inclination'
    cincl = cos inclination'
    sazim = sin azimuth'
    cazim = cos azimuth'

    azimuth'     = azimuth * τ
    inclination' = inclination * τ

type Buffer = Buffer.Allocated 'Buffer.Coherent Env

type Observer = Buffer.ObserverCoherent Env

newObserver
  :: ( Worker.HasOutput source
     , Worker.GetOutput source ~ Env
     )
  => source
  -> ResourceT (Engine.StageRIO st) Observer
newObserver envP = do
  initialEnv <- Worker.getOutputData envP
  Buffer.newObserverCoherent "Env" Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT 1 [initialEnv]

τ :: Float
τ = 2 * pi
