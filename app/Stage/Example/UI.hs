module Stage.Example.UI
  ( UI(..)
  , spawn

  , Observer(..)
  , newObserver
  , observe
  ) where

import RIO hiding (display)

import Control.Monad.Trans.Resource (ResourceT)
import Control.Monad.Trans.Resource qualified as Resource
import Geomancy (vec2, vec4)
import Resource.Region qualified as Region

import Engine.Camera qualified as Camera
import Engine.Types qualified as Engine
import Geomancy.Layout qualified as Layout
import Geomancy.Layout.Alignment qualified as Alignment
import Geomancy.Layout.Box (Box)
import Geomancy.Layout.Box qualified as Box
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Types (MonadVulkan)
import Engine.Worker qualified as Worker
import Render.Debug.Model qualified as Debug
import Render.Samplers qualified as Samplers
import Render.Unlit.Textured.Model qualified as UnlitTextured
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.Model.Observer qualified as Observer

import Global.Resource.Assets (Assets(..))
import Global.Resource.Font qualified as GameFont
import Global.Resource.Combined qualified as Combined
import Global.Resource.Texture qualified as Texture
import Stage.Example.World.Env qualified as Env

data UI = UI
  { display    :: Worker.Var Bool

  , showModels :: Worker.Var Bool
  , modelBBs   :: Worker.Var Bool
  , lightBBs   :: Worker.Var Bool

  , debugP    :: Worker.Merge Debug.Stores
  , debugBoxP :: Worker.Merge Box

  , davidP    :: Worker.Merge UnlitTextured.Stores
  , davidBoxP :: Worker.Merge Box

  , flareP    :: Worker.Merge UnlitTextured.Stores
  , statusTop :: Message.Process
  , statusBot :: Message.Process

  , fashionP :: Worker.Merge UnlitTextured.Stores
  }

spawn
  :: Assets
  -> Worker.Merge Box
  -> Camera.ViewProcess
  -> Worker.Var sceneInput
  -> Env.Process
  -> Engine.StageRIO env (Resource.ReleaseKey, UI)
spawn assets screenBoxP viewP _sceneV envP = Region.run do
  display <- Worker.newVar True

  showModels <- Worker.newVar True
  modelBBs <- Worker.newVar False
  lightBBs <- Worker.newVar False

  -- Main 12-columns with 2 sidebars

  leftP <- Worker.spawnMerge1
    (\parent -> Layout.placeSize Alignment.leftTop (parent.size * vec2 (2/12) 1) parent)
    screenBoxP
  centerP <- Worker.spawnMerge1
    (\parent -> Layout.placeSize Alignment.centerTop (parent.size * vec2 (4/12) 1) parent)
    screenBoxP
  rightP <- Worker.spawnMerge1
    (\parent -> Layout.placeSize Alignment.rightTop (parent.size * vec2 (2/12) 1) parent)
    screenBoxP

  -- Left sidebar

  debugBoxP <-
    Worker.spawnMerge1 (Layout.placeAspect Alignment.leftTop 1.0) leftP

  debugP <-
    Worker.spawnMerge1
      ( \box ->
          Debug.stores1
            (Samplers.linear Samplers.indices)
            (Combined.numTextures)
            [Box.mkTransform box]
      )
      debugBoxP

  davidBoxP <-
    Worker.spawnMerge1 (Layout.placeAspect Alignment.leftBottom 1.0) leftP

  davidP <-
    Worker.spawnMerge1
      ( \box ->
        UnlitTextured.stores1
          (Samplers.linear Samplers.indices)
          (Texture.david $ CombinedTextures.textures Combined.indices)
          [Box.mkTransform box]
      )
      davidBoxP

  -- Main container
  flareBoxP <-
    Worker.spawnMerge1 (Layout.placeAspect Alignment.center 1.0) centerP

  flareP <-
    Worker.spawnMerge1
      ( \box ->
          UnlitTextured.stores1
              (Samplers.linear Samplers.indices)
              (Texture.flare $ CombinedTextures.textures Combined.indices)
              [Box.mkTransform box]
      )
      flareBoxP

  padded <-
    Worker.spawnMerge1
      (Box.addPadding . Box.TRBL $ vec4 8 16 8 16)
      centerP
  -- Layout.padAbs centerP padVar

  statusTop <- Message.spawnFromR padded (Worker.getInput viewP) mkTopMessage
  statusBot <- Message.spawnFromR padded (Worker.getInput envP) mkBotMessage

  -- Right sidebar
  fashionBoxP <-
    Worker.spawnMerge1 (Layout.placeAspect Alignment.rightTop $ vec2 1.0 1.5) rightP

  fashionP <-
    Worker.spawnMerge1
      ( \box ->
          UnlitTextured.stores1
            (Samplers.linear Samplers.indices)
            (Texture.fashion $ CombinedTextures.textures Combined.indices)
            [Box.mkTransform box]
      )
      fashionBoxP

  pure UI{..}
  where
    mkTopMessage input@Camera.ViewOrbitalInput{} =
      small (vec4 0.5 1 0.25 1) Alignment.rightTop $
        fromString (show input)

    mkBotMessage input@Env.Input{} =
      small (vec4 0.5 1 0.25 1) Alignment.leftBottom $
        fromString (show input)

    small color origin text = Message.Input
      { inputText         = text
      , inputOrigin       = origin
      , inputSize         = 16
      , inputColor        = color
      , inputFont         = GameFont.small $ aFonts assets
      , inputFontId       = GameFont.small $ CombinedTextures.fonts Combined.indices
      , inputOutline      = vec4 0 0 0 0.5
      , inputOutlineWidth = 4/16
      , inputSmoothing    = 1/16
      }

data Observer = Observer
  { debug :: Debug.ObserverCoherent

  , david :: UnlitTextured.ObserverCoherent

  , flare    :: UnlitTextured.ObserverCoherent
  , messages :: [Message.Observer]

  , fashion :: UnlitTextured.ObserverCoherent
  }

newObserver :: UI -> ResourceT (Engine.StageRIO st) Observer
newObserver UI{..} = do
  -- XXX: Generic?
  messages <- traverse (const $ Message.newObserver 256)
    [ statusTop
    , statusBot
    ]

  debug <- Observer.newCoherent 1 "debug/depth"

  david <- Observer.newCoherent 1 "david"
  flare <- Observer.newCoherent 1 "flare"
  fashion <- Observer.newCoherent 1 "fashion"

  pure Observer{..}

{-# INLINE observe #-}
observe
  :: MonadVulkan env m
  => UI
  -> Observer
  -> m ()
observe UI{..} Observer{..} = do
  Observer.observeCoherent debugP debug

  Observer.observeCoherent davidP david
  Observer.observeCoherent flareP flare

  traverse_ (uncurry Message.observe) $
    zip [statusTop, statusBot] messages

  Observer.observeCoherent fashionP fashion
