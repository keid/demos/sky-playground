module Stage.Example.Events.Key
  ( callback
  , keyHandler
  ) where

import RIO

import UnliftIO.Resource (ReleaseKey)

import Engine.Events.Sink (MonadSink, Sink(..))
import Engine.Types (StageRIO)
import Engine.Window.Key (Key(..), KeyState(..))
import Engine.Window.Key qualified as Key

import Stage.Example.Events.Types (Event)
import Stage.Example.Events.Types qualified as Event
import Stage.Example.Types (RunState(..))

callback :: Sink Event RunState -> StageRIO RunState ReleaseKey
callback = Key.callback . keyHandler

keyHandler :: MonadSink RunState m => Sink Event RunState -> Key.Callback m
keyHandler (Sink signal) keyCode keyEvent@(_mods, state, key) = do
  logDebug $ "Key event (" <> display keyCode <> "): " <> displayShow keyEvent
  case key of
    Key'Space ->
      signal Event.DoNothing
    Key'F2 | pressed ->
      signal Event.ToggleUI
    _ ->
      pure ()
  where
    pressed = state == KeyState'Pressed
