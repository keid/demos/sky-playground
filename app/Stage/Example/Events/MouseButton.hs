module Stage.Example.Events.MouseButton
  ( clickHandler
  ) where

import RIO

import RIO.State (gets)

import Engine.Events.MouseButton (ClickHandler)
import Engine.Events.Sink (MonadSink, Sink(..))
import Engine.Window.MouseButton (MouseButton(..), MouseButtonState(..))
import Engine.Worker qualified as Worker
import Geomancy.Layout.Box (inside, projectInto)

import Stage.Example.Events.Types (Event)
import Stage.Example.Events.Types qualified as Event
import Stage.Example.Types (RunState(..))
import Stage.Example.UI qualified as UI

clickHandler :: MonadSink RunState m => ClickHandler Event RunState m
clickHandler (Sink signal) cursorPos buttonEvent = do
  ui <- gets rsUI
  uiDisplay <- Worker.getOutputData (UI.display ui)
  when uiDisplay do
    debugBox <- Worker.getOutputData (UI.debugBoxP ui)
    when (cursorPos `inside` debugBox) $
      case buttonEvent of
        (_mods, MouseButtonState'Pressed, MouseButton'1) -> do
          logInfo "Bugs? How unfortunate."
          signal Event.DoNothing
        _ ->
          logInfo $
            "MouseButton event: " <>
            displayShow
              ( cursorPos `projectInto` debugBox
              , buttonEvent
              )

    davidBox <- Worker.getOutputData (UI.davidBoxP ui)
    when (cursorPos `inside` davidBox) $
      case buttonEvent of
        (_mods, MouseButtonState'Pressed, MouseButton'1) -> do
          logInfo "David says nothing, it's a statue."
          signal Event.DoNothing
        (_mods, MouseButtonState'Released, MouseButton'1) -> do
          logInfo "David still says nothing, indifferent as a stone."
          signal Event.DoNothing
        _ ->
          logDebug "David isn't interested."
