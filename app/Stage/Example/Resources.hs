{-# LANGUAGE OverloadedLists #-}

module Stage.Example.Resources where

import RIO

import Control.Monad.Trans.Resource (ReleaseKey, ResourceT)
import Engine.Camera qualified as Camera
import Engine.Events.CursorPos qualified as CursorPos
import Engine.Types (StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Geomancy (vec3, vec4)
import Geomancy.Transform qualified as Transform
import GHC.Float (double2Float)
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.DescSets.Sun (SunInput(..), mkSun)
import Render.DescSets.Sun qualified as Sun
import Render.Pass.Offscreen qualified as Offscreen
import Render.ShadowMap.RenderPass qualified as ShadowPass
import Resource.Buffer qualified as Buffer
import Resource.CommandBuffer (withPools)
import Resource.Image qualified as Image
import Resource.Region qualified as Region
import RIO.State (gets)
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

import Global.Render qualified as Render
import Global.Render.EnvCube.DescSets qualified as EnvCube
import Global.Render.SkySun.DescSets qualified as SkySun
import Global.Resource.Assets (Assets(..))
import Global.Resource.Model qualified as GameModel
import Stage.Example.Types (FrameResources(..), RunState(..), RunState(..), FrameResources)
import Stage.Example.UI qualified as UI
import Stage.Example.World.Env qualified as Env
import Stage.Example.World.Scene qualified as WorldScene

initialRunState
  :: Assets
  -> Int
  -> StageRIO env (ReleaseKey, RunState)
initialRunState rsAssets thingCount = Region.run $ withPools \pools -> do
  Region.logDebug
    ("Setting up state for " <> display thingCount <> " things")
    ("Releasing state for " <> display thingCount <> " things")

  rsCursorP <- CursorPos.spawn

  let cameraTarget = vec3 0 0 0
  rsViewP <-
    Worker.spawnCell Camera.mkViewOrbital_ Camera.initialOrbitalInput
      { Camera.orbitAzimuth  = τ/8
      , Camera.orbitAscent   = τ/120
      , Camera.orbitDistance = 0.95
      , Camera.orbitScale    = 1000
      , Camera.orbitTarget   = cameraTarget
      }

  rsSunP <-
    Sun.spawn1 SunInput
      { siInclination = τ/8
      , siAzimuth     = -τ/8

        -- TODO: get from direction
      , siColor = vec4 1 1 1 1 -- vec4 1 0.75 0.66 4.0

        -- TODO: get from view frustum and scene BB
      , siRadius     = 2 ** (0.6692913 * 16)
      , siDepthRange = 2 ** (0.7637795 * 16)
      , siSize       = 768
      , siTarget     = 0

      , siShadowIx   = 0
      }

  rsPerspectiveP <- Camera.spawnPerspective
  rsSceneV <- Worker.newVar WorldScene.initialInput
  rsSceneP <- Worker.spawnMerge3 WorldScene.mkScene rsPerspectiveP rsViewP rsSceneV

  ortho <- Camera.spawnOrthoPixelsCentered
  rsSceneUiP <- Worker.spawnMerge1 WorldScene.mkSceneUi ortho

  rsEnvP <-
    Env.spawn Env.Input
      { azimuth      = 70/360
      , inclination  = -0/360
      , altitude     = 0.5
      , earthRadius  = 6360.0
      , atmoHeight   = 60.0
      , atmoRayleigh = 6.0
      , atmoMie      = 1.2
      }

  rsScreenBoxP <- Camera.trackOrthoPixelsCentered

  rsUI <- Region.local $ UI.spawn rsAssets rsScreenBoxP rsViewP rsSceneV rsEnvP

  rsUpdateSkyP <- Worker.spawnMerge2 (\_env _sun -> ()) rsEnvP rsSunP
  rsUpdateSky <- Worker.newObserverIO ()

  rsBattleInstance <- Buffer.createStaged
    (Just "rsBattleInstance")
    pools
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    [mempty]
  Region.local_ $ Buffer.register rsBattleInstance

  pure RunState{..}

initialFrameResources
  :: Queues Vk.CommandPool
  -> Render.RenderPasses
  -> Render.Pipelines
  -> ResourceT (StageRIO RunState) FrameResources
initialFrameResources _pools Render.RenderPasses{..} pipelines = do
  -- XXX: shadowmapping stuff
  (frSunDescs, frSunData) <- Sun.createSet0Ds (Render.getSunLayout pipelines)
  frSunOut <- Worker.newObserverIO mempty

  combinedTextures <- gets $ aTextures . rsAssets
  combinedCubes <- gets $ aCubeMaps . rsAssets

  combinedMaterials <- gets $ aMaterials . rsAssets
  materials <- Region.local $
    Buffer.allocateCoherent
      (Just "combinedMaterials")
      Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT
      1
      combinedMaterials

  let
    -- XXX: match with Global.Render.allocatePipelines ⚠️
    extendedTextures =
      mappend
        (toList combinedTextures)
        [ Offscreen.colorTexture rpStatic
        ]

    materialCubes =
      mappend
        (toList combinedCubes)
        [ Offscreen.colorCube rpEnvIrradiance
        ]

    skyboxCubes =
      mappend
        (toList combinedCubes)
        [ Offscreen.colorCube rpEnvCube -- XXX: same index, different cube (hires)
        ]

    shadows =
      [ Image.aiImageView . ShadowPass.smDepthImage $
          Basic.rpShadowPass rpBasic
      ]

  set0 <- Set0.allocate
    (Render.getSceneLayout pipelines)
    extendedTextures
    materialCubes
    (Just frSunData)
    shadows
    (Just materials)

  let sceneLayouts = Pipeline.pDescLayouts (Render.pEnvCube pipelines)
  set1 <- SkySun.createSet1 sceneLayouts

  frEnv <- gets rsEnvP >>= Env.newObserver
  Worker.readObservedIO frEnv >>= SkySun.updateSet1 set1

  set2 <- EnvCube.createSet2 sceneLayouts
  mapperData <- EnvCube.createMapper
  Region.local_ $ Buffer.register mapperData
  EnvCube.updateSet2 set2 mapperData

  let
    frScene =
      set0 `Set0.extendResourceDS`
      set1 `Set0.extendResourceDS`
      set2

  set0sky <- Set0.allocate
    (Render.getSceneLayout pipelines)
    extendedTextures
    skyboxCubes
    (Just frSunData)
    shadows
    (Just materials)
  let
    frSceneCube =
      set0sky `Set0.extendResourceDS`
      set1 `Set0.extendResourceDS`
      set2

  set0ui <- Set0.allocate
    (Render.getSceneLayout pipelines)
    extendedTextures
    combinedCubes
    (Just frSunData)
    [ Image.aiImageView . ShadowPass.smDepthImage $
        Basic.rpShadowPass rpBasic
    ]
    (Just materials)

  let
    frSceneUi =
      set0ui `Set0.extendResourceDS`
      set1 `Set0.extendResourceDS`
      set2

  frWorldThings <- Buffer.newObserverCoherent
    "frWorldThings"
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    10
    mempty

  let
    frEnvCube =
      set0sky `Set0.extendResourceDS`
      set1 `Set0.extendResourceDS`
      set2

  set0i1 <- Set0.allocate
    (Render.getSceneLayout pipelines)
    Nothing
    (Just $ Offscreen.colorCube rpEnvCube)
    Nothing
    []
    Nothing
  let
    frEnvIrradiance =
      set0i1 `Set0.extendResourceDS`
      set1 `Set0.extendResourceDS`
      set2

  logReleaseThings <- toIO $ logDebug "Releasing Example recycled resources"
  Region.register_ logReleaseThings

  ui <- gets rsUI
  frUI <- UI.newObserver ui

  frBasic <- Basic.allocateObservers rpBasic (Render.pBasicExt pipelines)

  (_roomMeta, Storable.length -> roomNodes, _roomModel) <- gets $ GameModel.vikingRoom . aModels . rsAssets
  frRoomInstance <- Region.local $
    Buffer.allocateCoherent
      (Just "frRoomInstance")
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      roomNodes
      (Storable.replicate roomNodes mempty)

  -- TODO: extract copypasta
  (_homeMeta, Storable.length -> homeNodes, _homeModel) <- gets $ GameModel.skyHome . aModels . rsAssets
  frSkyHomeInstance <- Region.local $
    Buffer.allocateCoherent
      (Just "frSkyHomeInstance")
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      homeNodes
      (Storable.replicate homeNodes mempty)

  pure FrameResources{..}

updateBuffers
  :: RunState
  -> FrameResources
  -> Render.StageFrameRIO FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Set0.observe rsSceneP frScene
  Set0.observe rsSceneP frSceneCube
  Set0.observe rsSceneUiP frSceneUi

  Worker.observeIO_ rsSunP frSunOut \_oldBBs (sunBB, sun) -> do
    -- XXX: must stay the same or descsets must be updated with a new buffer
    _same <- Buffer.updateCoherent [sun, staticMoon] frSunData
    pure [sunBB, moonBB]

  Buffer.observeCoherentSingle rsEnvP frEnv

  UI.observe rsUI frUI

  Engine.Frame{fRenderpass, fPipelines} <- asks snd
  Basic.observePipelines (Render.rpBasic fRenderpass) (Render.pBasicExt fPipelines) frBasic

  --  XXX: those should be running as workers, but anyway...
  t <- fmap double2Float getMonotonicTime

  -- Animate the viking room

  let
    roomTransform = mconcat
      [ Transform.rotateY $ -τ * t / 128
      , Transform.translate
          250
          (10 * cos (τ * t / 33))
          0
      ]
  void $! Buffer.updateCoherent
    [roomTransform] -- XXX: assuming there's  only one instance (see below)
    frRoomInstance

  -- Animate the sky home

  let
    homeTransform = mconcat
      [ Transform.rotateY $ τ * t / 128
      , Transform.translate
          (-250)
          (10 * sin (τ * t / 33))
          0
      ]

  void $! -- XXX: assuming the buffer wouldn't be reallocated
    Buffer.updateCoherent
      (Storable.replicate (fromIntegral $ Buffer.aUsed frSkyHomeInstance) homeTransform) -- XXX: assuming the buffer is initialized with correct size
      frSkyHomeInstance

  where
    -- TODO: actually a backlight, depends on Env location
    (moonBB, staticMoon) = mkSun SunInput
      { siColor       = vec4 0.125 0.25 1 1 -- vec4 0.0625 0.0625 0.125 1
      , siInclination = -0.25 * τ + 1/128
      , siAzimuth     = 0
      , siRadius      = 768

      , siTarget     = 0
      , siDepthRange = 768 * 2
      , siSize       = 768

      , siShadowIx   = 1
      }

τ :: Float
τ = 2 * pi
