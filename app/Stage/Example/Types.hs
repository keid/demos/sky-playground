module Stage.Example.Types where

import RIO

import Data.Tagged (Tagged)
import Engine.Camera qualified as Camera
import Engine.Events.CursorPos qualified as CursorPos
import Engine.Worker (ObserverIO)
import Engine.Worker qualified as Worker
import Geomancy (Transform)
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.DescSets.Sun (Sun)
import Render.DescSets.Sun qualified as Sun
import Resource.Buffer qualified as Buffer
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

import Global.Render qualified as Render
import Global.Render.EnvCube.DescSets (Mapper)
import Global.Resource.Assets (Assets)
import Stage.Example.UI (UI)
import Stage.Example.UI qualified as UI
import Stage.Example.World.Env (Env)
import Stage.Example.World.Env qualified as Env
import Stage.Example.World.Scene qualified as Scene

type Stage = Render.Stage FrameResources RunState
type Frame = Render.Frame FrameResources
type Scene = Render.StageScene RunState FrameResources
type RecordCommands a = Vk.CommandBuffer -> FrameResources -> Word32 -> Render.StageFrameRIO FrameResources RunState a

data FrameResources = FrameResources
  { frWorldThings :: ObserverIO (Buffer.Allocated 'Buffer.Coherent Transform)

  , frSunDescs :: Tagged '[Sun] (Vector Vk.DescriptorSet)
  , frSunData  :: Buffer.Allocated 'Buffer.Coherent Sun
  , frSunOut   :: ObserverIO (Storable.Vector Transform)

  , frScene         :: Set0.FrameResource '[Set0.Scene, Env, Mapper]
  , frSceneCube     :: Set0.FrameResource '[Set0.Scene, Env, Mapper]
  , frEnvCube       :: Set0.FrameResource '[Set0.Scene, Env, Mapper]
  , frEnvIrradiance :: Set0.FrameResource '[Set0.Scene, Env, Mapper]
  , frSceneUi       :: Set0.FrameResource '[Set0.Scene, Env, Mapper]

  , frEnv :: Env.Observer

  , frUI :: UI.Observer
  , frBasic :: Basic.PipelineObservers

  , frRoomInstance :: Buffer.Allocated 'Buffer.Coherent Transform
  , frSkyHomeInstance :: Buffer.Allocated 'Buffer.Coherent Transform
  }

data RunState = RunState
  { rsPerspectiveP :: Camera.ProjectionProcess 'Camera.Perspective
  , rsViewP        :: Camera.ViewProcess

  , rsCursorP   :: CursorPos.Process

  , rsSceneP :: Scene.Process
  , rsSceneV :: Scene.InputVar

  , rsSceneUiP :: Scene.Process

  , rsEnvP :: Env.Process
  , rsSunP :: Sun.Process
  -- , rsMoonP       :: Sun.Process
  -- , rsLightsP     :: Sun.LightsProcess

  , rsAssets :: Assets

  , rsUI :: UI

  , rsUpdateSkyP :: Worker.Merge ()
  , rsUpdateSky :: ObserverIO ()

  , rsBattleInstance :: Buffer.Allocated 'Buffer.Staged Transform
  }
