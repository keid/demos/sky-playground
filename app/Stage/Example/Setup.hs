module Stage.Example.Setup
  ( stackStage
  ) where

import RIO

import Engine.Events qualified as Events
import Engine.Events.CursorPos qualified as CursorPos
import Engine.Events.MouseButton qualified as MouseButton
import Engine.Stage.Component qualified as Stage
import Engine.Types (StackStage(..))
import Render.ImGui qualified as ImGui
import Resource.Region qualified as Region
import RIO.State (gets)

import Global.Render qualified
import Global.Resource.Assets (Assets(..))
import Stage.Example.Events.Key qualified as Key
import Stage.Example.Events.Kontrol qualified as Kontrol
import Stage.Example.Events.MouseButton qualified as MouseButton
import Stage.Example.Events.Sink (handleEvent)
import Stage.Example.Resources qualified as Resources
import Stage.Example.Scene qualified as Scene
import Stage.Example.Scene.Shadows qualified as Shadows
import Stage.Example.Scene.Sky qualified as Sky
import Stage.Example.Types (Stage, RunState(..), FrameResources)

stackStage
  :: Assets
  -> Int -> StackStage
stackStage assets =
  StackStage . exampleStage assets

exampleStage
  :: Assets
  -> Int -> Stage.Example.Types.Stage
exampleStage assets moonCount =
  Stage.assemble
    ("Example [" <> textDisplay moonCount <> "]")
    rendering
    (resources assets moonCount)
    [ setupEvents (exampleStage assets), updateBuffers, Sky.scene, scene ]

rendering :: Stage.Rendering Global.Render.RenderPasses Global.Render.Pipelines st
rendering = Stage.Rendering
  { rAllocateRP = Global.Render.allocateRenderPass
  , rAllocateP = Global.Render.allocatePipelines True
  }

resources
  :: Assets
  -> Int
  -> Stage.Resources Global.Render.RenderPasses Global.Render.Pipelines RunState FrameResources
resources assets moonCount = Stage.Resources
  { rInitialRS = Resources.initialRunState assets moonCount
  , rInitialRR = Resources.initialFrameResources
  }

setupEvents
  :: (Int -> Stage.Example.Types.Stage)
  -> Stage.Scene Global.Render.RenderPasses Global.Render.Pipelines RunState FrameResources
setupEvents mkSelf = mempty
  { Stage.scBeforeLoop = do
      cursor <- gets rsCursorP
      -- cursorWindow <- gets rsCursorPos
      -- cursorCentered <- gets rsCursorP
      void $! Region.local $ Events.spawn
        (handleEvent mkSelf)
        [ CursorPos.callback cursor
        , MouseButton.callback cursor MouseButton.clickHandler
        , Key.callback
        , Kontrol.spawn
        ]
  }

-- XXX: This hints that updateBuffers should be a part of Resource instead as it bridges RS and FR data.
updateBuffers :: Stage.Scene Global.Render.RenderPasses Global.Render.Pipelines RunState FrameResources
updateBuffers = mempty
  { Stage.scUpdateBuffers = Resources.updateBuffers
  }

scene :: Stage.Scene Global.Render.RenderPasses Global.Render.Pipelines RunState FrameResources
scene = mempty
  { Stage.scBeforeLoop =
      ImGui.allocateLoop True
  , Stage.scRecordCommands =
      -- TODO: declare a render graph
      \cb fr ii -> do
          (vertexOnly, forward) <- Scene.recordCommands cb fr ii
          Shadows.recordCommands vertexOnly cb fr ii
          forward
  }
