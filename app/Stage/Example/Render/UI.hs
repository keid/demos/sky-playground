{-# LANGUAGE OverloadedLists #-}

module Stage.Example.Render.UI where

import RIO

import DearImGui qualified
import Geomancy (vec4, withVec4)
-- import Geomancy.Transform qualified as Transform
-- import GHC.Float (double2Float)
import RIO.State (gets)
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.DescSets (Bound, Compatible)
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Sun (SunInput(..))
import Render.Draw qualified as Draw
import Render.ImGui qualified as ImGui
-- import Resource.Buffer qualified as Buffer

import Global.Render qualified as Render
import Global.Resource.Assets (Assets(..))
import Global.Resource.Model qualified as GameModel
import Stage.Example.Types (FrameResources(..), RunState(..))
import Stage.Example.UI qualified as UI
import Stage.Example.World.Env qualified as Env

type DrawM = Render.StageFrameRIO FrameResources RunState

type DrawData dsl m =
  ( Bool
  , Vk.CommandBuffer -> Bound dsl Void Void m ()
  , Vk.CommandBuffer -> Bound dsl Void Void m ()
  )

prepareUI
  :: ( Compatible '[Scene] dsl
     )
  => Render.Pipelines
  -> UI.Observer
  -> DrawM (DrawData dsl DrawM)
prepareUI Render.Pipelines{pBasic} rUI = do
  ui <- gets rsUI
  uiDisplay <- Worker.getOutputData (UI.display ui)

  if not uiDisplay then
    pure
      ( uiDisplay
      , const $ pure ()
      , const $ pure ()
      )
  else do
    debug <- Worker.readObservedIO (UI.debug rUI)
    david <- Worker.readObservedIO (UI.david rUI)
    _flare <- Worker.readObservedIO (UI.flare rUI)
    uiMessages <- traverse Worker.readObservedIO (UI.messages rUI)
    fashion <- Worker.readObservedIO (UI.fashion rUI)

    quadUV <- gets $ GameModel.quadUV . aModels . rsAssets
    _bbWire <- gets $ GameModel.bbWire . aModels . rsAssets

{-
    cur <- gets rsCursorP >>= Worker.getOutputData
    t <- fmap double2Float getMonotonicTime
    let
      uis =
        withVec2 cur \x y ->
          [ mconcat
              [ Transform.scale3 (sin t * 32 + 64) (sin t * 32 + 64) 0.25
              , Transform.rotateZ (t / 2)
              , Transform.translate x y 0.5
              ]
          , mconcat
              [ Transform.scale3 (cos t * 24 + 48) (cos t * 24 + 48) 0.25
              , Transform.rotateZ (-t / 3)
              , Transform.translate x y 0.5
              ]
          ]
    bbInstancesUi <- Buffer.allocateCoherent
      (Just "bbInstancesUi")
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      1
      uis
-}

    let
      uiDrawOpaque cb = do -- when uiDisplay do
        -- Vk.cmdSetScissor cb 0 [Layout.boxRectAbs leftBox]
        Graphics.bind cb (Basic.pUnlitTextured pBasic) do
          Draw.indexed cb quadUV david
          Draw.indexed cb quadUV fashion

      uiDrawBlended cb = do
        Graphics.bind cb (Basic.pDebugShadow pBasic) $
          Draw.indexed cb quadUV debug

        Graphics.bind cb (Basic.pEvanwSdf pBasic) $
          traverse_ (Draw.quads cb) uiMessages

        -- Graphics.bind cb (Render.pWireframeNoDepth fPipelines) $
        --   Draw.indexed cb bbWire bbInstancesUi

    pure
      ( uiDisplay
      , uiDrawOpaque
      , uiDrawBlended
      )

imguiDrawData :: DrawM DearImGui.DrawData
imguiDrawData = fmap snd $ ImGui.mkDrawData do
  sunP <- gets rsSunP
  envP <- gets rsEnvP
  ui <- gets rsUI
  -- let
  --   minSize = pure @IO (DearImGui.ImVec2 200 200)
  --   maxSize = pure @IO (DearImGui.ImVec2 500 500)
  -- DearImGui.setNextWindowSizeConstraints minSize maxSize
  DearImGui.withWindowOpen "Success" do
    DearImGui.newLine
    DearImGui.text "Warning: Avoiding success may incur some costs."
    DearImGui.newLine
    void $! DearImGui.checkbox "Avoid" $
      Worker.stateVar (UI.display ui)

  DearImGui.withWindowOpen "Draw stuff" do
    void $! DearImGui.checkbox "Models" $
      Worker.stateVar (UI.showModels ui)

    void $! DearImGui.checkbox "Model bounding boxes" $
      Worker.stateVar (UI.modelBBs ui)

    void $! DearImGui.checkbox "Light bounding boxes" $
      Worker.stateVar (UI.lightBBs ui)

  DearImGui.withWindowOpen "Environment" do
    void $! DearImGui.sliderFloat "Orbit Azimuth, turn"
      (Worker.stateVarMap
        Env.azimuth
        (\new env -> env { Env.azimuth = new })
        envP
      )
      0
      1

    void $! DearImGui.sliderFloat "Orbit Inclination, turn"
      (Worker.stateVarMap
        Env.inclination
        (\new env -> env { Env.inclination = new })
        envP
      )
      0
      1

    void $! DearImGui.sliderFloat "Orbit Altitude, km"
      (Worker.stateVarMap
        Env.altitude
        (\new env -> env { Env.altitude = new })
        envP
      )
      0.0125
      (2**20)

    DearImGui.separator

    -- XXX: derived from planet-sun position
    void $! DearImGui.sliderFloat "Sun azimuth, turn"
      (Worker.stateVarMap
        ((/ τ) . siAzimuth)
        (\new si -> si { siAzimuth = new * τ })
        sunP
      )
      0
      1

    void $! DearImGui.sliderFloat "Sun inclination, turn"
      (Worker.stateVarMap
        ((/ τ) . siInclination)
        (\new si -> si { siInclination = new * τ })
        sunP
      )
      0
      1

    void $! DearImGui.colorButton
      "Local color"
      (Worker.stateVarMap
        (\SunInput{siColor} ->
            withVec4 siColor DearImGui.ImVec4
        )
        (\(DearImGui.ImVec4 r g b a) si -> si
            { siColor = vec4 r g b a
            }
        )
        sunP
      )

τ :: Float
τ = 2 * pi
