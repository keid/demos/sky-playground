module Stage.Example.Scene.Shadows where

import RIO

import Data.Type.Equality (type (~))
import Engine.Types qualified as Engine
import Engine.Vulkan.DescSets (withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (Bound(..))
import Render.Basic qualified as Basic
import Render.Pass (usePass)
import Render.ShadowMap.RenderPass qualified as ShadowPass
import Resource.Model qualified as Model
import Vulkan.Core10 qualified as Vk

import Global.Render qualified as Render
import Stage.Example.Types (FrameResources(..), RunState(..))
import Geomancy (Transform)

recordCommands
  :: m ~ Render.StageFrameRIO FrameResources RunState
  => (Vk.CommandBuffer -> Bound ds (Model.Vertex3d ()) Transform m ())
  -> Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> m ()
recordCommands vertexOnly cb FrameResources{..} imageIndex = do
  Engine.Frame{fRenderpass, fPipelines} <- asks snd
  let
    Render.RenderPasses{rpBasic} = fRenderpass
    Render.Pipelines{pBasic=Basic.Pipelines{..}} = fPipelines

  let shadowRender = Basic.rpShadowPass rpBasic
  let shadowLayout = Pipeline.pLayout pShadowCast
  usePass shadowRender imageIndex cb do
    -- XXX: per-light/layer
    let
      viewport = ShadowPass.smRenderArea shadowRender
      scissor  = ShadowPass.smRenderArea shadowRender
    Swapchain.setDynamic cb viewport scissor
    withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS shadowLayout frSunDescs $
      Graphics.bind cb pShadowCast do
        -- XXX: coerce descriptor sets
        let Bound impredicative = vertexOnly cb
        Bound impredicative
