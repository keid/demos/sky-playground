module Stage.Example.Scene.Sky where

import RIO

import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 qualified as Set0
import Render.Draw qualified as Draw
import Render.Pass (usePass)
import Render.Pass.Offscreen qualified as Offscreen
import RIO.State (gets)

import Global.Render qualified as Render
import Stage.Example.Types (FrameResources(..), RunState(..), RecordCommands, Scene)

scene :: Scene
scene = mempty
  { Stage.scRecordCommands = recordCommands
  }

recordCommands :: RecordCommands ()
recordCommands cb FrameResources{..} imageIndex = do
  updater <- gets rsUpdateSkyP
  updateNeeded <- gets rsUpdateSky
  Worker.observeIO_ updater updateNeeded \() () -> do
    Engine.Frame{fRenderpass, fPipelines} <- asks snd
    let
      Render.RenderPasses{rpEnvCube, rpEnvIrradiance, rpStatic} = fRenderpass
      Render.Pipelines{..} = fPipelines

    usePass rpStatic imageIndex cb do
      let
        viewport = Offscreen.oRenderArea rpStatic
        scissor  = Offscreen.oRenderArea rpStatic
      Swapchain.setDynamic cb viewport scissor

      Set0.withBoundSet0 frScene pEnvCube cb do
        Graphics.bind cb pSkySunOffscreen $
          Draw.triangle_ cb

    usePass rpEnvCube imageIndex cb do
      let
        viewport = Offscreen.oRenderArea rpEnvCube
        scissor  = Offscreen.oRenderArea rpEnvCube
      Swapchain.setDynamic cb viewport scissor
      Set0.withBoundSet0 frScene pEnvCube cb $
        Graphics.bind cb pEnvCube $
          Draw.triangle_ cb

    usePass rpEnvIrradiance imageIndex cb do
      let
        viewport = Offscreen.oRenderArea rpEnvIrradiance
        scissor  = Offscreen.oRenderArea rpEnvIrradiance
      Swapchain.setDynamic cb viewport scissor
      Set0.withBoundSet0 frEnvIrradiance pEnvIrradiance cb $
        Graphics.bind cb pEnvIrradiance $
          Draw.triangle_ cb
