{-# LANGUAGE OverloadedLists #-}

module Stage.Example.Scene where

import RIO

import Data.Vector.Storable qualified as Storable
import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (Bound(..))
import Engine.Worker qualified as Worker
import Geomancy (Transform)
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.Draw qualified as Draw
import Render.ImGui qualified as ImGui
import Render.Pass (usePass)
import Resource.Buffer qualified as Buffer
import Resource.Mesh.Lit qualified as Lit
import Resource.Mesh.Types (Meta(..))
import Resource.Mesh.Types qualified as Mesh
import Resource.Model qualified as Model
import RIO.State (gets)
import Vulkan.Core10 qualified as Vk

import Global.Render qualified as Render
import Global.Resource.Assets (Assets(..))
import Global.Resource.Model qualified as GameModel
import Stage.Example.Render.UI qualified as RenderUI
import Stage.Example.Types (FrameResources(..), RunState(..), RecordCommands)
import Stage.Example.UI qualified as UI

recordCommands
  :: RecordCommands
    ( Vk.CommandBuffer -> Bound ds (Model.Vertex3d ()) Transform (Render.StageFrameRIO FrameResources RunState) ()
    , Render.StageFrameRIO FrameResources RunState ()
    )
recordCommands cb FrameResources{..} imageIndex = do
  Engine.Frame{fSwapchainResources, fRenderpass, fPipelines} <- asks snd
  let
    Render.RenderPasses{..} = fRenderpass
    Render.Pipelines{pBasic=Basic.Pipelines{..}, ..} = fPipelines

  thingsModel <- gets $ GameModel.icosphere1 . aModels . rsAssets
  bbWire <- gets $ GameModel.bbWire . aModels . rsAssets
  thingsInstances <- Worker.readObservedIO frWorldThings

  (roomMeta, _rNodes, roomModel) <- gets $ GameModel.vikingRoom . aModels . rsAssets

  sunBBs <- Worker.readObservedIO frSunOut

  -- Battle

  (battleMeta, bNodes, battleModel) <- gets $ GameModel.battle . aModels . rsAssets
  let
    battleTransform = mempty -- XXX: staged buffers are un-peek-able
    battleOpaqueNodes = fromIntegral . Model.irIndexCount $ Mesh.mOpaqueNodes battleMeta

  -- SkyHome

  (homeMeta, hNodes, homeModel) <- gets $ GameModel.skyHome . aModels . rsAssets
  homeTransform <- Buffer.peekCoherent 0 frSkyHomeInstance >>=
    maybe (error "the instance is present") pure
  let
    homeOpaqueNodes = fromIntegral . Model.irIndexCount $ Mesh.mOpaqueNodes homeMeta
    homeRanges = map Mesh.getRange $ Storable.toList hNodes

  -- Room

  roomTransform <- Buffer.peekCoherent 0 frRoomInstance >>=
    maybe (error "the instance is present") pure

  -- Bounding box wireframes

  ui <- gets rsUI
  modelBBs <- Worker.getOutputData $ UI.modelBBs ui
  lightBBs <- Worker.getOutputData $ UI.lightBBs ui
  (_bbKey, bbInstances) <-
    Buffer.allocateCoherent (Just "bbInstances") Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1 $
      mappend
        ( if not lightBBs then
            mempty
          else
            sunBBs
        )
        ( if not modelBBs then
            mempty
          else
            Storable.concat
              [ Storable.map
                  (\Lit.MaterialNode{mnNode} -> Mesh.nTransformBB mnNode <> battleTransform)
                  (Storable.drop battleOpaqueNodes bNodes)

              , Storable.map
                  (\Lit.MaterialNode{mnNode} -> Mesh.nTransformBB mnNode <> homeTransform)
                  (Storable.drop homeOpaqueNodes hNodes)

              , [ mconcat
                    [ mTransformBB battleMeta
                    , battleTransform
                    ]
                , mconcat
                    [ mTransformBB homeMeta
                    , homeTransform
                    ]
                , mconcat
                    [ mTransformBB roomMeta
                    , roomTransform
                    ]
                ]
              ]
        )

  (uiDisplay, uiDrawOpaque, uiDrawBlended) <- RenderUI.prepareUI fPipelines frUI

  dear <- RenderUI.imguiDrawData

  let
    vertexOnly :: Vk.CommandBuffer -> Bound ds (Model.Vertex3d ()) Transform (Render.StageFrameRIO FrameResources RunState) ()
    vertexOnly cb'  = do
        -- XXX: Cast shadows only from the opaque bits
        battleInstance <- Bound $ gets rsBattleInstance
        Draw.indexedPosRanges cb' battleModel battleInstance [mOpaqueIndices battleMeta]

        -- TODO: well... textured models can have cut-outs that really should be considered w/discard()
        Draw.indexedPosRanges cb' homeModel frSkyHomeInstance $
          take homeOpaqueNodes homeRanges

        -- XXX: the rest are opaque
        Draw.indexedPosRanges cb' roomModel frRoomInstance [mOpaqueIndices roomMeta]
        Draw.indexedPos cb' thingsModel thingsInstances

    forward :: Render.StageFrameRIO FrameResources RunState ()
    forward =
      -- let sceneLayout = Pipeline.pLayout pSkySun
      usePass (Basic.rpForwardMsaa rpBasic) imageIndex cb do
        Swapchain.setDynamicFullscreen cb fSwapchainResources

        -- XXX: Run opaque UI first to depth-test away costly scene fragments.
        when uiDisplay $
          Set0.withBoundSet0 frSceneUi pEnvCube cb do
            uiDrawOpaque cb

        showModels <- Worker.getOutputData $ UI.showModels ui
        -- XXX: proceed with the scene...
        Set0.withBoundSet0 frScene pEnvCube cb do
          when showModels do
            -- XXX: Depth-prepass, opaque only
            Graphics.bind cb pDepthOnly do
              vertexOnly cb

            -- XXX: opaque first
            Worker.readObservedIO (Basic.pLitColored frBasic) >>= \(_key, pipeline) ->
              Graphics.bind cb pipeline do
                Draw.indexed cb thingsModel thingsInstances

            Worker.readObservedIO (Basic.pLitMaterial frBasic) >>= \(_key, pipeline) ->
              Graphics.bind cb pipeline do
                Draw.indexed cb roomModel frRoomInstance

                battleInstance <- Bound $ gets rsBattleInstance
                Draw.indexedRanges cb battleModel battleInstance
                  [mOpaqueIndices battleMeta]

                Draw.indexedParts True cb homeModel frSkyHomeInstance 0 $
                  take homeOpaqueNodes homeRanges

        -- -- XXX: hack scene descsets - rebind hires skybox temporarily
        Set0.withBoundSet0 frSceneCube pEnvCube cb do
          -- XXX: draw skybox after all opaque things
          Graphics.bind cb pSkybox $
            Draw.triangle_ cb

        -- XXX: unhack scene descsets
        Set0.withBoundSet0 frScene pEnvCube cb do
          -- XXX: draw the blending parts now
          -- TODO: those are really should be depth-sorted wrt. camera position

          -- XXX: uses different vertex topology: edges instead of tris
          Graphics.bind cb pWireframe $
            Draw.indexed cb bbWire bbInstances

          when showModels do
            -- Graphics.bind cb pLitMaterialBlend do
            Worker.readObservedIO (Basic.pLitMaterialBlend frBasic) >>= \(_key, pipeline) ->
              Graphics.bind cb pipeline do
                -- XXX: skipping room, no blended nodes
                battleInstance <- Bound $ gets rsBattleInstance
                Draw.indexedRanges cb battleModel battleInstance [mBlendedIndices battleMeta]
                Draw.indexedParts True cb homeModel frSkyHomeInstance homeOpaqueNodes homeRanges

        -- XXX: Draw blended UI parts
        when uiDisplay $
          Set0.withBoundSet0 frSceneUi pEnvCube cb do
          -- withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS sceneLayout rSceneUiDescs $
            uiDrawBlended cb

        ImGui.draw dear cb

  pure (vertexOnly, forward)

τ :: Float
τ = 2 * pi
