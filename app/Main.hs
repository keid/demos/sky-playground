module Main (main) where

import RIO hiding (traceM)

import Debug.Trace (traceM)
import Engine.App (engineMainWith)
import RIO.Directory (doesDirectoryExist, getCurrentDirectory, withCurrentDirectory)
import RIO.FilePath (takeDirectory, takeFileName, (</>))
import Stage.Loader.Setup qualified as Loader
import System.Environment (getExecutablePath)

import Global.Resource.Assets qualified as Assets
import Global.Resource.Font qualified as Font
import Global.Resource.Texture qualified as Texture
import Paths_keid_demo_skyplayground (getDataDir)
import Stage.Example.Setup qualified as Example

main :: IO ()
main = do
  currentDir <- getCurrentDirectory

  executableDir <- fmap takeDirectory getExecutablePath
  let shareDir = takeDirectory executableDir </> "share"

  usrShareName <- fmap takeFileName getDataDir
  let
    withVer = usrShareName
    sansVer = stripVersion withVer

  let
    candidates =
      [ currentDir
      , shareDir </> withVer
      , shareDir </> sansVer
      ]
  exists <- for candidates \dir ->
    doesDirectoryExist (dir </> "resources")
  case filter fst (zip exists candidates) of
    (True, found) : _rest ->
      withCurrentDirectory found $
        engineMainWith handoff action
    _ -> do
      traceM "Resource directory not found:"
      traverse_ traceM candidates
      exitFailure
  where
    (handoff, action) =
      Loader.bootstrap
        "Sky Playground"
        (Font.small Font.configs, Font.large Font.configs)
        (splash, spinner)
        Assets.load
        (\assets -> Example.stackStage assets 6)

    splash = Texture.stars Texture.sources
    spinner = Texture.vulkan_planet Texture.sources

stripVersion :: String -> String
stripVersion = reverse . drop 1 . dropWhile (/= '-') . reverse
