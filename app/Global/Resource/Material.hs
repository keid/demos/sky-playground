module Global.Resource.Material
  ( MaterialCollection
  ) where

import RIO.Vector.Storable qualified as Storable

import Render.Lit.Material (Material)

type MaterialCollection = Storable.Vector Material
