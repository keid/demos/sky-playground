-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.CubeMap
  ( Collection(..)
  , CubeMapCollection
  , Ids
  , Textures
  , sources
  , indices
  , numCubes
  ) where

import RIO

import GHC.Generics (Generic1)
import Global.Resource.CubeMap.Base qualified as Base

import Resource.Collection (Generically1(..), enumerate, size)
import Resource.Source (Source(..))
import Resource.Static as Static
import Resource.Texture (Texture, CubeMap)

data Collection a = Collection
  { base     :: Base.Collection a
  , test     :: a
  , planet   :: a
  , clouds   :: a
  , milkyway :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic, Generic1)
  deriving Applicative via (Generically1 Collection)

type CubeMapCollection = Collection (Int32, Texture CubeMap)
type Ids               = Collection Int32
type Textures          = Collection (Texture CubeMap)

Static.filePatterns Static.Files "resources/cubemaps"

sources :: Collection Source
sources = Collection
  { base     = Base.sources
  , test     = File Nothing TEST_KTX2
  , planet   = File Nothing PLANET_KTX2
  , clouds   = File Nothing CLOUDS_KTX2
  , milkyway = File Nothing MILKYWAY_KTX2
  }

indices :: Collection Int32
indices = fmap fst $ enumerate sources

numCubes :: Num a => a
numCubes = size sources
