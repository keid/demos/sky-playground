-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.Texture
  ( Collection(..)
  , sources
  ) where

import RIO

import GHC.Generics (Generic1)
import Global.Resource.Texture.Base qualified as Base
import Resource.Collection (Generically1(..))
import Resource.Source (Source(..))
import Resource.Static as Static

import Global.Resource.Texture.SkyHome qualified as SkyHome

data Collection a = Collection
  { base :: Base.Collection a

  , uvgrid :: a

  , brickwall        :: a
  , brickwall_normal :: a

  , cd      :: a
  , david   :: a
  , fashion :: a
  , flare   :: a
  , stars   :: a

  , vulkan_planet :: a
  , viking_room   :: a
  , sky_home      :: SkyHome.Collection a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via (Generically1 Collection)

Static.filePatterns Static.Files "resources/textures/playground"

sources :: Collection Source
sources = Collection
  { base = Base.sources

  , uvgrid = File Nothing UVGRID_KTX2

  , brickwall        = File Nothing BRICKWALL_KTX2
  , brickwall_normal = File Nothing BRICKWALL_NORMAL_KTX2

  , cd      = File Nothing CD_KTX2
  , david   = File Nothing DAVID_KTX2
  , fashion = File Nothing FASHION_KTX2
  , flare   = File Nothing FLARE_KTX2
  , stars   = File Nothing STARS_KTX2

  , vulkan_planet = File Nothing VULKAN_PLANET_KTX2

  , viking_room = File Nothing VIKING_ROOM_KTX2
  , sky_home    = SkyHome.sources
  }
