module Global.Resource.Texture.SkyHome
  ( Collection(..)
  , sources
  ) where

import RIO

import GHC.Generics (Generic1)
import Resource.Collection (Generically1(..))
import Resource.Source as Source
import Resource.Static as Static

{- |
  Textures from the sky_home.gltf scene

  Names could be anything, but the order must match source "images" array.
-}
data Collection a = Collection
  { bark_emissive               :: a
  , bark_basecolor              :: a
  , material_413_basecolor      :: a
  , door__barrels_basecolor     :: a
  , roof_basecolor              :: a
  , material_650_basecolor      :: a
  , wooden_skel_no_op_basecolor :: a
  , wall_basecolor              :: a
  , op_branches_basecolor       :: a
  , planks_basecolor            :: a
  , grass_basecolor             :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via (Generically1 Collection)

Static.filePatterns Static.Files "resources/textures/sky_home"

sources :: Collection Source
sources = Collection
  { bark_emissive               = File Nothing BARK_EMISSIVE_KTX2
  , bark_basecolor              = File Nothing BARK_BASECOLOR_KTX2
  , material_413_basecolor      = File Nothing MATERIAL_413_BASECOLOR_KTX2
  , door__barrels_basecolor     = File Nothing DOOR__BARRELS_BASECOLOR_KTX2
  , roof_basecolor              = File Nothing ROOF_BASECOLOR_KTX2
  , material_650_basecolor      = File Nothing MATERIAL_650_BASECOLOR_KTX2
  , wooden_skel_no_op_basecolor = File Nothing WOODEN_SKEL_NO_OP_BASECOLOR_KTX2
  , wall_basecolor              = File Nothing WALL_BASECOLOR_KTX2
  , op_branches_basecolor       = File Nothing OP_BRANCHES_BASECOLOR_KTX2
  , planks_basecolor            = File Nothing PLANKS_BASECOLOR_KTX2
  , grass_basecolor             = File Nothing GRASS_BASECOLOR_KTX2
  }
