{-# LANGUAGE OverloadedLists #-}

{-# OPTIONS_GHC -fplugin Foreign.Storable.Generic.Plugin #-}

module Global.Render.EnvCube.DescSets
  ( Mapper(..)
  , createMapper
  , set2
  , createSet2
  , updateSet2
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import Foreign.Storable.Generic (GStorable)
import Geomancy (Transform, vec3)
import Geomancy.Transform qualified as Transform
import Geomancy.Vulkan.Projection qualified as Projection
import Geomancy.Vulkan.View qualified as View
import RIO.Vector.Partial qualified as Vector
import Vulkan.Core10 qualified as Vk
import Vulkan.CStruct.Extends (SomeStruct(..))
import Vulkan.Zero (Zero(..))

import Engine.Types (StageRIO)
import Engine.Vulkan.Types (DsLayoutBindings, MonadVulkan, getDevice)
import Resource.Buffer qualified as Buffer
import Resource.Region qualified as Region
import Resource.Vulkan.DescriptorPool qualified as DescriptorPool


data CubeViews a = CubeViews
  { cvRight  :: a
  , cvLeft   :: a
  , cvBottom :: a
  , cvTop    :: a
  , cvFront  :: a
  , cvBack   :: a
  } deriving (Show, Functor, Generic)

instance GStorable (CubeViews Transform)

cubeViews :: CubeViews Transform
cubeViews = CubeViews
  { cvRight  = View.lookAt axisRight  0 axisTop
  , cvLeft   = View.lookAt axisLeft   0 axisTop
  , cvTop    = View.lookAt axisTop    0 axisBack
  , cvBottom = View.lookAt axisBottom 0 axisFront
  , cvFront  = View.lookAt axisFront  0 axisTop
  , cvBack   = View.lookAt axisBack   0 axisTop
  }
  where
    axisRight  = vec3   1   0   0
    axisLeft   = vec3 (-1)  0   0
    axisTop    = vec3   0 (-1)  0
    axisBottom = vec3   0   1   0
    axisFront  = vec3   0   0   1
    axisBack   = vec3   0   0 (-1)

cubeViewsInverse :: CubeViews Transform
cubeViewsInverse = fmap Transform.inverse cubeViews

mapper :: Mapper
mapper = Mapper
  { mInvProjection = Transform.inverse $ Projection.perspective @Word (pi/2) (1/2048) 1 512 512
  , mInvViews      = cubeViewsInverse
  }

data Mapper = Mapper
  { mInvProjection :: Transform
  , mInvViews      :: CubeViews Transform
  } deriving (Show, Generic)

instance GStorable Mapper

createMapper
  :: MonadVulkan env m
  => m (Buffer.Allocated 'Buffer.Coherent Mapper)
createMapper =
  Buffer.createCoherent
    (Just "EnvCube.mapper")
    Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT
    1
    [mapper]

set2 :: Tagged Mapper DsLayoutBindings
set2 = Tagged
  [ (set2bind0, zero)
  ]

set2bind0 :: Vk.DescriptorSetLayoutBinding
set2bind0 = Vk.DescriptorSetLayoutBinding
  { binding           = 0
  , descriptorType    = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
  , descriptorCount   = 1
  , stageFlags        = Vk.SHADER_STAGE_ALL
  , immutableSamplers = mempty
  }

createSet2
  :: Tagged (set0 ': env ': Mapper ': next) (Vector Vk.DescriptorSetLayout)
  -> ResourceT (StageRIO st) (Tagged Mapper Vk.DescriptorSet)
createSet2 (Tagged layouts) = do
  descPool <- Region.local $
    DescriptorPool.allocate (Just "EnvCube") 1
      [ ( Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
        , 1
        )
      ]

  device <- asks getDevice
  descSets <- Vk.allocateDescriptorSets device $ zero
    { Vk.descriptorPool = descPool
    , Vk.setLayouts     = [layouts Vector.! 1]
    }
  fmap (Tagged @Mapper) $ Vector.headM descSets

updateSet2
  :: MonadVulkan env m
  => Tagged Mapper Vk.DescriptorSet
  -> Buffer.Allocated stage Mapper
  -> m ()
updateSet2 (Tagged descSet) mapperData = do
  device <- asks getDevice
  Vk.updateDescriptorSets
    device
    [ writeSet2b0 descSet mapperData
    ]
    mempty

writeSet2b0 :: Vk.DescriptorSet -> Buffer.Allocated stage Mapper -> SomeStruct Vk.WriteDescriptorSet
writeSet2b0 destSet1 mapperData = SomeStruct zero
  { Vk.dstSet          = destSet1
  , Vk.dstBinding      = 0
  , Vk.dstArrayElement = 0
  , Vk.descriptorCount = 1
  , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
  , Vk.bufferInfo      = [bind0]
  }
  where
    bind0 = Vk.DescriptorBufferInfo
      { Vk.buffer = Buffer.aBuffer mapperData
      , Vk.offset = 0
      , Vk.range  = Vk.WHOLE_SIZE
      }
