{-# LANGUAGE OverloadedLists #-}

module Global.Render.EnvIrradiance.Pipeline
  ( Pipeline
  , allocate
  , Config
  , config
  ) where

import RIO

import Control.Monad.Trans.Resource (MonadResource, ResourceT)
import Data.Tagged (Tagged(..))
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (DsLayoutBindings, HasRenderPass(..), MonadVulkan)
import Render.Code (compileVert, compileFrag)
import Render.DescSets.Set0 (Scene)
import Resource.Region qualified as Region
import Vulkan.Core10 qualified as Vk

import Global.Render.EnvCube.DescSets (Mapper)
import Global.Render.EnvIrradiance.Code qualified as Code
import Stage.Example.World.Env (Env)

type Pipeline = Graphics.Pipeline '[Scene, Env, Mapper] () ()
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

allocate
  :: ( MonadVulkan env m
     , MonadResource m
     , HasRenderPass renderpass
     )
  => Vk.SampleCountFlagBits
  -> Tagged Scene DsLayoutBindings
  -> Tagged Env DsLayoutBindings
  -> Tagged Mapper DsLayoutBindings
  -> renderpass
  -> ResourceT m Pipeline
allocate multisample set0 set1 set2 =
  Region.local . Graphics.allocate
    Nothing
    multisample
    (config set0 set1 set2)

config
  :: Tagged Scene DsLayoutBindings
  -> Tagged Env DsLayoutBindings
  -> Tagged Mapper DsLayoutBindings
  -> Config
config (Tagged set0) (Tagged set1) (Tagged set2) =
    Graphics.baseConfig
      { Graphics.cStages       = Graphics.basicStages vertSpirv fragSpirv
      , Graphics.cDescLayouts  = Tagged @'[Scene, Env, Mapper] [set0, set1, set2]
      , Graphics.cCull         = Vk.CULL_MODE_NONE
      }

vertSpirv :: ByteString
vertSpirv = $(compileVert Code.vert)

fragSpirv :: ByteString
fragSpirv = $(compileFrag Code.frag)
