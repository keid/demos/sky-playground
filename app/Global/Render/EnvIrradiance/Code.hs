module Global.Render.EnvIrradiance.Code
  ( vert
  , frag
  ) where

import RIO

import Global.Render.EnvCube.Code (set2binding0)
import Render.Code (Code, glsl)
import Render.DescSets.Set0.Code (set0binding1, set0binding3)

vert :: Code
vert = fromString
  [glsl|
    #version 450
    #extension GL_EXT_multiview : enable

    ${set2binding0}

    layout(location = 0) out vec3 fUVW;

    float farZ = 0.9999; // 1 - 1e-7;

    void main() {
      vec4 pos = vec4(0.0);
      switch(gl_VertexIndex) {
          case 0: pos = vec4(-1.0,  3.0, farZ, 1.0); break;
          case 1: pos = vec4(-1.0, -1.0, farZ, 1.0); break;
          case 2: pos = vec4( 3.0, -1.0, farZ, 1.0); break;
      }

      gl_Position = pos;

      vec3 unProjected = (mapper.invProjection * pos).xyz * vec3(-1.0);
      fUVW = mat3(mapper.invViews[gl_ViewIndex]) * unProjected;
    }
  |]

frag :: Code
frag = fromString
  [glsl|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable
    #extension GL_EXT_nonuniform_qualifier : enable

    ${set0binding1}
    ${set0binding3}

    layout(location = 0) in vec3 fragUVW;

    layout(location = 0) out vec4 outColor;

    const float M_TAU = 6.283185307;

    vec3 diffuse_convolution(vec3 normal) {
      vec3 acc = vec3(0.0);
      float samples = 0.0;

      float theta_ = atan(normal.x, normal.y);
      float phi_ = acos(normal.z);

      const float sideSteps = 64;
      const float stepSize = M_TAU / 1024.0; // TODO: derive from texture size?

      for(float azimuth = -sideSteps; azimuth <= sideSteps; azimuth++) {
        for(float inclination = -sideSteps; inclination <= sideSteps; inclination++) {
          float deltaPhi   = float(azimuth)     * stepSize;
          float deltaTheta = float(inclination) * stepSize;

          float phi   = phi_   + deltaPhi;
          float theta = theta_ + deltaTheta;

          vec3 sample_dir = vec3(
            sin(theta) * sin(phi),
            cos(theta) * sin(phi),
            cos(phi)
          );

          acc += texture(
            samplerCube(
              cubes[nonuniformEXT(0)],
              samplers[3] // linear
            ),
            sample_dir
          ).rgb * vec3(
            abs(1.0 - azimuth / sideSteps) *
            abs(1.0 - inclination / sideSteps)
          );

          samples += 1.0;
        }
      }

      return (acc / samples);
    }

    void main() {
      outColor = vec4(
        diffuse_convolution(normalize(fragUVW)),
        1.0
      );
    }
  |]