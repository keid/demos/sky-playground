{-# LANGUAGE OverloadedLists #-}

module Global.Render.SkySun.DescSets
  ( set1
  , createSet1
  , updateSet1
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import RIO.Vector.Partial qualified as Vector
import Vulkan.Core10 qualified as Vk
import Vulkan.CStruct.Extends (SomeStruct(..))
import Vulkan.Zero (Zero(..))

import Engine.Vulkan.Types (DsLayoutBindings, HasVulkan(..))
import Engine.Types (StageRIO)
import Resource.Buffer qualified as Buffer
import Resource.Region qualified as Region
import Resource.Vulkan.DescriptorPool qualified as DescriptorPool

import Stage.Example.World.Env (Env)

set1 :: Tagged Env DsLayoutBindings
set1 = Tagged
  [ (set1bind0, zero)
  ]

set1bind0 :: Vk.DescriptorSetLayoutBinding
set1bind0 = Vk.DescriptorSetLayoutBinding
  { binding           = 0
  , descriptorType    = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
  , descriptorCount   = 1
  , stageFlags        = Vk.SHADER_STAGE_ALL
  , immutableSamplers = mempty
  }

createSet1
  :: Tagged (set0 ': Env ': next) (Vector Vk.DescriptorSetLayout)
  -> ResourceT (StageRIO st) (Tagged Env Vk.DescriptorSet)
createSet1 (Tagged layouts) = do
  descPool <- Region.local $
    DescriptorPool.allocate
      (Just "SkySun")
      1
      [ ( Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
        , 1
        )
      ]

  device <- asks getDevice
  descSets <- Vk.allocateDescriptorSets device $ zero
    { Vk.descriptorPool = descPool
    , Vk.setLayouts     = [layouts Vector.! 1]
    }
  fmap (Tagged @Env) $ Vector.headM descSets

updateSet1
  :: ( MonadIO m
     , MonadReader env m
     , HasVulkan env
     , HasCallStack
     )
  => Tagged Env Vk.DescriptorSet
  -> Buffer.Allocated 'Buffer.Coherent Env
  -> m ()
updateSet1 (Tagged descSet) envData = do
  Buffer.peekCoherent 0 envData >>= \case
    Just _ok ->
      pure ()
    Nothing ->
      throwString "updateSet1 got an empty buffer"
  device <- asks getDevice
  Vk.updateDescriptorSets
    device
    [ writeSet1b0 descSet envData
    ]
    mempty

writeSet1b0 :: Vk.DescriptorSet -> Buffer.Allocated stage Env -> SomeStruct Vk.WriteDescriptorSet
writeSet1b0 destSet1 envData = SomeStruct zero
  { Vk.dstSet          = destSet1
  , Vk.dstBinding      = 0
  , Vk.dstArrayElement = 0
  , Vk.descriptorCount = 1
  , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
  , Vk.bufferInfo      = [bind0]
  }
  where
    bind0 = Vk.DescriptorBufferInfo
      { Vk.buffer = Buffer.aBuffer envData
      , Vk.offset = 0
      , Vk.range  = Vk.WHOLE_SIZE
      }
