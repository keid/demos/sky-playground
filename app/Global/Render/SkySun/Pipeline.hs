{-# LANGUAGE OverloadedLists #-}

-- XXX: A cubed version is forked into EnvCube.

module Global.Render.SkySun.Pipeline
  ( Pipeline
  , allocate
  , Config
  , config
  ) where

import RIO

import Control.Monad.Trans.Resource (MonadResource, ResourceT)
import Data.Tagged (Tagged(..))
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (DsLayoutBindings, HasRenderPass(..), MonadVulkan)
import Render.Code (compileVert, compileFrag)
import Render.DescSets.Set0 (Scene)
import Resource.Region qualified as Region
import Vulkan.Core10 qualified as Vk

import Global.Render.SkySun.Code qualified as Code
import Stage.Example.World.Env (Env)

type Pipeline = Graphics.Pipeline '[Scene, Env] () ()
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

allocate
  :: ( MonadVulkan env m
     , MonadResource m
     , HasRenderPass renderpass
     )
  => Vk.SampleCountFlagBits
  -> Tagged Scene DsLayoutBindings
  -> Tagged Env DsLayoutBindings
  -> renderpass
  -> ResourceT m Pipeline
allocate multisample set0 set1 =
  Region.local .
    Graphics.allocate
      Nothing
      multisample
      (config set0 set1)

config
  :: Tagged Scene DsLayoutBindings
  -> Tagged Env DsLayoutBindings
  -> Config
config (Tagged set0) (Tagged set1) =
    Graphics.baseConfig
      { Graphics.cStages       = Graphics.basicStages vertSpirv fragSpirv
      , Graphics.cDescLayouts  = Tagged @'[Scene, Env] [set0, set1]
      , Graphics.cCull         = Vk.CULL_MODE_NONE
      }

vertSpirv :: ByteString
vertSpirv = $(compileVert Code.vert)

fragSpirv :: ByteString
fragSpirv = $(compileFrag Code.frag)
