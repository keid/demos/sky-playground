module Global.Render
  ( Stage
  , Frame
  , StageScene
  , StageFrameRIO

  , RenderPasses(..)
  , allocateRenderPass

  , Pipelines(..)
  , allocatePipelines

  , getSceneLayout
  , getSunLayout
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import Engine.Stage.Component qualified as Stage
import Engine.Types (StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (HasSwapchain, RenderPass(..))
import Render.Basic qualified as Basic
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Set0 qualified as Scene
import Render.DescSets.Sun (Sun)
import Render.ImGui qualified as ImGui
import Render.Pass.Offscreen (Offscreen)
import Render.Pass.Offscreen qualified as Offscreen
import Render.Samplers qualified as Samplers
import Vulkan.Core10 qualified as Vk

import Global.Render.EnvCube.DescSets qualified as EnvCube
import Global.Render.EnvCube.Pipeline qualified as EnvCube
import Global.Render.EnvIrradiance.Pipeline qualified as EnvIrradiance
import Global.Render.SkySun.DescSets qualified as SkySun
import Global.Render.SkySun.Pipeline qualified as SkySun
import Global.Resource.Combined qualified as Combined
import Global.Resource.CubeMap qualified as CubeMap

type Stage = Engine.Stage RenderPasses Pipelines
type Frame = Engine.Frame RenderPasses Pipelines
type StageScene = Stage.Scene RenderPasses Pipelines
type StageFrameRIO fr rs = Engine.StageFrameRIO RenderPasses Pipelines fr rs

data RenderPasses = RenderPasses
  { rpBasic         :: Basic.RenderPasses
  , rpStatic        :: Offscreen
  , rpEnvCube       :: Offscreen
  , rpEnvIrradiance :: Offscreen
  } deriving (Generic)

instance RenderPass RenderPasses where
  updateRenderpass swapchain RenderPasses{..} = RenderPasses
    <$> updateRenderpass swapchain rpBasic
    <*> pure rpStatic
    <*> pure rpEnvCube
    <*> pure rpEnvIrradiance

  refcountRenderpass RenderPasses{..} = do
    refcountRenderpass rpBasic
    refcountRenderpass rpStatic
    refcountRenderpass rpEnvCube
    refcountRenderpass rpEnvIrradiance

settingsBasic :: Basic.Settings
settingsBasic = Basic.Settings
  { sShadowSize   = 2048 * 4
  , sShadowLayers = 2
  }

settingsStatic :: HasSwapchain swapchain => swapchain -> Offscreen.Settings
settingsStatic swapchain = Offscreen.Settings
  { sLabel =
      "Static"
  , sLayers =
      1
  , sMultiView =
      False
  , sSamples =
      Vk.SAMPLE_COUNT_2_BIT
  , sExtent =
      Vk.Extent2D 720 360
  , sFormat =
      Vk.FORMAT_R16G16B16A16_SFLOAT
  , sColorLayout =
      Just Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
  , sDepthFormat =
      Swapchain.getDepthFormat swapchain
  , sDepthLayout =
      Nothing
  , sMipMap =
      False
  }

settingsEnvCube :: HasSwapchain swapchain => swapchain -> Offscreen.Settings
settingsEnvCube swapchain = Offscreen.Settings
  { sLabel =
      "EnvCube"
  , sLayers =
      6
  , sMultiView =
      True
  , sSamples =
      Vk.SAMPLE_COUNT_1_BIT
  , sExtent =
      Vk.Extent2D side side
  , sFormat =
      Vk.FORMAT_R16G16B16A16_SFLOAT
  , sColorLayout =
      Just Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
  , sDepthFormat =
      Swapchain.getDepthFormat swapchain
  , sDepthLayout =
      Nothing
  , sMipMap =
      False
  }
  where
    -- XXX: limited by sample count
    side =
      -- 512 * 4 * 4
      512 * 4
      -- 512 `div` 4

settingsEnvIrradiance :: HasSwapchain swapchain => swapchain -> Offscreen.Settings
settingsEnvIrradiance swapchain = (settingsEnvCube swapchain)
  { Offscreen.sLabel  = "EnvIrradiance"
  , Offscreen.sExtent = Vk.Extent2D 16 16
  }

data Pipelines = Pipelines
  { pBasic           :: Basic.Pipelines
  , pBasicExt        :: Basic.PipelineWorkers
  , pSkySun          :: SkySun.Pipeline
  , pSkySunOffscreen :: SkySun.Pipeline
  , pEnvCube         :: EnvCube.Pipeline
  , pEnvIrradiance   :: EnvIrradiance.Pipeline
  }

allocateRenderPass
  :: HasSwapchain swapchain
  => swapchain
  -> ResourceT (StageRIO st) RenderPasses
allocateRenderPass swapchain = do
  rpBasic <- Basic.allocate settingsBasic swapchain

  rpStatic        <- Offscreen.allocate $ settingsStatic swapchain
  rpEnvCube       <- Offscreen.allocate $ settingsEnvCube swapchain
  rpEnvIrradiance <- Offscreen.allocate $ settingsEnvIrradiance swapchain

  pure RenderPasses{..}

allocatePipelines
  :: HasSwapchain swapchain
  => Bool
  -> swapchain
  -> RenderPasses
  -> ResourceT (StageRIO st) Pipelines
allocatePipelines useImgui swapchain RenderPasses{..} = do
  let msaa = Swapchain.getMultisample swapchain

  samplers <- Samplers.allocate (Swapchain.getAnisotropy swapchain)
  let
    -- XXX: match with Stage.Example.Render.initialData ⚠️
    sceneBinds =
      Scene.mkBindings
        samplers
        ( () -- rpStatic
        : replicate (length Combined.sources) ()
        )
        ( () -- rpEnvIrradiance OR rpEnvCube
        : replicate (length CubeMap.sources) ()
        )
        2

  pBasic <- Basic.allocatePipelines sceneBinds msaa rpBasic
  pBasicExt <- Basic.allocateWorkers sceneBinds msaa rpBasic

  pSkySun <- SkySun.allocate msaa sceneBinds SkySun.set1 (Basic.rpForwardMsaa rpBasic)

  pSkySunOffscreen <- SkySun.allocate
    (Offscreen.sSamples $ settingsStatic swapchain)
    sceneBinds
    SkySun.set1
    rpStatic

  pEnvCube <- EnvCube.allocate
    (Offscreen.sSamples $ settingsEnvCube swapchain)
    sceneBinds
    SkySun.set1
    EnvCube.set2
    rpEnvCube

  pEnvIrradiance <- EnvIrradiance.allocate
    (Offscreen.sSamples $ settingsEnvIrradiance swapchain)
    sceneBinds
    SkySun.set1
    EnvCube.set2
    rpEnvIrradiance

  when useImgui $
    void $! ImGui.allocate swapchain (Basic.rpForwardMsaa rpBasic) 0

  pure Pipelines{..}

getSceneLayout :: Pipelines -> Tagged '[Scene] Vk.DescriptorSetLayout
getSceneLayout = Basic.getSceneLayout . pBasic

getSunLayout :: Pipelines -> Tagged '[Sun] Vk.DescriptorSetLayout
getSunLayout = Basic.getSunLayout . pBasic
